package com.digital.doctor.ddchat.exception;

/**
 * Исключение возникающее по причине отсутствия пользователя с данным идентификатором.
 */
public class UserNotFoundException extends CustomException {

    public UserNotFoundException(Integer userId) {
        super("Пользователь с id=" + userId + " не найден");
    }

}
