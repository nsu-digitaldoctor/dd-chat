package com.digital.doctor.ddchat.exception;

/**
 * Исключение возникающее по причине отсутствия у пользователя прав на совершение операции с определенным обсуждением.
 */
public class UserChatPermissionException extends CustomException {

    private static final String MESSAGE_TEMPLATE =
            "Пользователь с id=%d не может выполнять операцию %s над обсуждением с id=%d";

    public UserChatPermissionException(Integer userId, Integer chatId, String operation) {
        super(
                String.format(MESSAGE_TEMPLATE, userId, operation, chatId)
        );
    }

    public UserChatPermissionException(String message) {
        super(message);
    }


}
