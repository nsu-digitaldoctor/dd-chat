package com.digital.doctor.ddchat.exception;

/**
 * Исключение возникающее по причине отсутствия обсуждения с данным идентификатором.
 */
public class ChatNotFoundException extends CustomException {

    public ChatNotFoundException(Integer chatId) {
        super("Обсуждение с id=" + chatId + " не найден");
    }

}