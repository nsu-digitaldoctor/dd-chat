package com.digital.doctor.ddchat.exception;

import com.digital.doctor.ddchat.model.enums.ChatStatus;

/**
 * Исключение возникающее по причине невозможного изменения статуса обсуждения.
 */
public class ChatTransitionException extends CustomException {

    public ChatTransitionException(ChatStatus chatStatus, ChatStatus newStatus) {
        super(
                String.format(
                        "Невозможен переход из статуса %s в статус %s",
                        chatStatus.name(),
                        newStatus.name()
                )
        );
    }

    public ChatTransitionException(Boolean isAnonymous, Boolean newIsAnonymous) {
        super(
                String.format(
                        "Невозможен переход из статуса анонимности %s в статус анонимности %s",
                        isAnonymous.toString(),
                        newIsAnonymous.toString()
                )
        );
    }

    public ChatTransitionException(Integer userId) {
        super(
                String.format(
                        "Невозможно указать пользователя с id=%d в качестве врача.",
                        userId
                )
        );
    }

}
