package com.digital.doctor.ddchat.exception;

import com.digital.doctor.ddchat.model.enums.UserRole;

/**
 * Исключение возникающее по причине отсутствия у пользователя с определенной ролью прав на совершение операции.
 */
public class UserRoleException extends CustomException {

    private static final String MESSAGE_TEMPLATE = "Пользователь с ролью %s не может выполнять операцию %s";

    public UserRoleException(UserRole role, String operation) {
        super(
                String.format(MESSAGE_TEMPLATE, role.name(), operation)
        );
    }

    public UserRoleException(String message) {
        super(message);
    }

}
