package com.digital.doctor.ddchat.exception;

/**
 * Исключение сервиса dd-chat.
 */
public class CustomException extends RuntimeException {

    protected CustomException(Throwable throwable) {
        super(throwable);
    }

    protected CustomException(String message) {
        super(message);
    }

    protected CustomException(String message, Throwable throwable) {
        super(message, throwable);
    }

}
