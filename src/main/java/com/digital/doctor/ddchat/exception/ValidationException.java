package com.digital.doctor.ddchat.exception;

import com.networknt.schema.ValidationMessage;

import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class ValidationException extends CustomException {

    public ValidationException(Set<ValidationMessage> errors) {
        super(
                "Возникли ошибки валидации: " +
                        errors.stream()
                                .map(Objects::toString)
                                .collect(Collectors.joining(", "))
        );
    }

    public ValidationException(Throwable throwable) {
        super(throwable);
    }

}
