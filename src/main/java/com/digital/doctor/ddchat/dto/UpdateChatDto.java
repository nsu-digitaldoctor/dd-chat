package com.digital.doctor.ddchat.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * DTO запроса на обновление статуса обсуждения.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UpdateChatDto {

    /**
     * Список изменений.
     */
    private List<ChatStatusChangeDto> chats;

    /**
     * Пользователь обновляющий статусы.
     */
    private UserDto user;

}
