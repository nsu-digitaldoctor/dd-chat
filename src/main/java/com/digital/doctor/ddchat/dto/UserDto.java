package com.digital.doctor.ddchat.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * DTO сущности пользователь.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserDto {

    /**
     * Идентификатор в системе хранения.
     */
    private Integer id;

    private String firstName;
    private String middleName;
    private String lastName;

    /**
     * Роль пользователя в системе.
     */
    private String role;

}
