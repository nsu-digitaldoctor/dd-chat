package com.digital.doctor.ddchat.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.time.OffsetDateTime;

/**
 * DTO сущности обсуждение.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ChatDto {

    /**
     * Идентификатор в системе хранения.
     */
    private Integer id;

    /**
     * Пользователь создавший обсуждение.
     */
    private UserDto user;

    /**
     * Название обсуждения.
     */
    private String title;

    /**
     * Признак является ли обсуждение анонимным.
     */
    private Boolean isAnonymous;

    /**
     * Статус обсуждения.
     */
    private String status;

    /**
     * Дата создания.
     */
    private OffsetDateTime createDate;

    /**
     * Доктор решивший обсуждение.
     */
    private UserDto doctor;

}
