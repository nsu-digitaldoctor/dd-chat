package com.digital.doctor.ddchat.dto;

import lombok.*;

/**
 * DTO ошибки выполнения запроса.
 */
@Getter
@Setter
@AllArgsConstructor
@Builder
public class StatusDto {

    private final String error;

    private final String description;

}
