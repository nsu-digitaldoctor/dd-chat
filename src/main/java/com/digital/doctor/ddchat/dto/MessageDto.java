package com.digital.doctor.ddchat.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.OffsetDateTime;

@AllArgsConstructor
@Builder
@Getter
@Setter
public class MessageDto {

    private final Integer messageId;

    private final Integer chatId;

    private final UserDto user;

    private OffsetDateTime sentDate;

    private final String content;

    private final Integer replyMessageId;

}
