package com.digital.doctor.ddchat.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

/**
 * DTO запроса создания обсуждения.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CreateChatDto {

    /**
     * Параметры создаваемого обсуждения.
     */
    private ChatDto chat;

    /**
     * Пользователь создающий обсуждение.
     */
    private UserDto user;

}
