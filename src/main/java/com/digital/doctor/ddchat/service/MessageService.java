package com.digital.doctor.ddchat.service;

import com.digital.doctor.ddchat.model.Chat;
import com.digital.doctor.ddchat.model.Message;
import com.digital.doctor.ddchat.model.User;

import java.util.List;
import java.util.Optional;

public interface MessageService {

    Optional<Message> createMessage(Message message, User user);

    void deleteMessage(Message message, User user);

    List<Message> getMessages(Chat chat, User user);

}
