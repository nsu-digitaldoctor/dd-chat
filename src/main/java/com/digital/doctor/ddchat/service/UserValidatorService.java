package com.digital.doctor.ddchat.service;

import com.digital.doctor.ddchat.exception.CustomException;
import com.digital.doctor.ddchat.exception.UserNotFoundException;
import com.digital.doctor.ddchat.model.Chat;
import com.digital.doctor.ddchat.model.Message;
import com.digital.doctor.ddchat.model.User;
import com.digital.doctor.ddchat.model.enums.OperationType;

/**
 * Класс для работы проверки возможности совершения операций пользователем.
 */
public interface UserValidatorService {

    void validateUserRole(User user, OperationType operationType) throws UserNotFoundException;

    void validateUserChatPermissions(User user, Chat chat, OperationType operationType) throws CustomException;

    void validateUserChatStatus(
            User user, Message message, Chat chat, OperationType operationType
    ) throws CustomException;

}
