package com.digital.doctor.ddchat.service;

import com.digital.doctor.ddchat.dao.ChatDao;
import com.digital.doctor.ddchat.dao.MessageDao;
import com.digital.doctor.ddchat.dao.filter.MessageFilter;
import com.digital.doctor.ddchat.model.Chat;
import com.digital.doctor.ddchat.model.Message;
import com.digital.doctor.ddchat.model.User;
import com.digital.doctor.ddchat.model.enums.ChatStatus;
import com.digital.doctor.ddchat.model.enums.OperationType;
import com.digital.doctor.ddchat.model.enums.UserRole;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
@Slf4j
public class MessageServiceImpl implements MessageService {

    private final UserValidatorService userValidatorService;

    private final MessageDao messageDao;

    private final ChatDao chatDao;

    @Override
    public Optional<Message> createMessage(Message message, User user) {
        try {
            userValidatorService.validateUserChatStatus(user, message, message.getChat(), OperationType.CREATE_MESSAGE);
            prepareMessageForCreation(message, user);
            var saved = messageDao.save(message);
            if (user.getRole() == UserRole.DOCTOR) {
                updateChatStatus(saved);
            }
            return Optional.ofNullable(saved);
        } catch (Exception exception) {
            log.error("Ошибка при создании обсуждения: " + exception.getMessage());
            throw exception;
        }
    }

    @Override
    public void deleteMessage(Message message, User user) {
        try {
            userValidatorService.validateUserChatStatus(user, message, message.getChat(), OperationType.DELETE_MESSAGE);
            messageDao.delete(message.getId());
        } catch (Exception exception) {
            log.error("Ошибка при создании обсуждения: " + exception.getMessage());
            throw exception;
        }
    }

    @Override
    public List<Message> getMessages(Chat chat, User user) {
        try {
            var existingChat = chatDao.getChat(chat.getId());
            userValidatorService.validateUserChatStatus(user, null, existingChat, OperationType.GET_MESSAGES);

            var filter = MessageFilter.builder().chatId(chat.getId()).build();
            return messageDao.getMessages(filter);
        } catch (Exception exception) {
            log.error("Ошибка при получении сообщений: " + exception.getMessage());
            throw exception;
        }
    }

    private void prepareMessageForCreation(Message message, User user) {
        message.setId(null);
        message.setUser(user);
    }

    private void updateChatStatus(Message message) {
        var chat = message.getChat();
        if (chat.getStatus() == ChatStatus.NEW) {
            chat.setStatus(ChatStatus.ANSWERED);
            chatDao.save(chat);
        }
    }

}
