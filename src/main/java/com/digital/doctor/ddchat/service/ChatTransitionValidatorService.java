package com.digital.doctor.ddchat.service;

import com.digital.doctor.ddchat.model.Chat;

/**
 * Класс для работы проверки возможности совершения операций обновления над обсуждением.
 */
public interface ChatTransitionValidatorService {

    void validateTransition(Chat newChat);

}
