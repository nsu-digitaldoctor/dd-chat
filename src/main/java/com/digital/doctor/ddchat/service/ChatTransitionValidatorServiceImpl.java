package com.digital.doctor.ddchat.service;

import com.digital.doctor.ddchat.dao.ChatDao;
import com.digital.doctor.ddchat.dao.UserDao;
import com.digital.doctor.ddchat.exception.ChatTransitionException;
import com.digital.doctor.ddchat.model.Chat;
import com.digital.doctor.ddchat.model.User;
import com.digital.doctor.ddchat.model.enums.ChatStatus;
import com.digital.doctor.ddchat.model.enums.UserRole;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.EnumMap;
import java.util.List;
import java.util.Map;

@Service
@AllArgsConstructor
public class ChatTransitionValidatorServiceImpl implements ChatTransitionValidatorService {

    private static final Map<ChatStatus, List<ChatStatus>> validTransitions;

    private final ChatDao chatDao;

    private final UserDao userDao;

    static {
        validTransitions = new EnumMap<>(ChatStatus.class);
        validTransitions.put(
                ChatStatus.NEW,
                List.of(ChatStatus.CLOSED)
        );
        validTransitions.put(
                ChatStatus.ANSWERED,
                List.of(ChatStatus.RESOLVED, ChatStatus.CLOSED)
        );
    }

    @Override
    public void validateTransition(Chat newChat) {
        var chat = chatDao.getChat(newChat.getId());
        validateStatus(chat.getStatus(), newChat.getStatus());
        validateIsAnonymous(chat.getIsAnonymous(), newChat.getIsAnonymous());
        validateDoctor(newChat.getStatus(), newChat.getDoctor());
    }

    private void validateStatus(ChatStatus chatStatus, ChatStatus newStatus) {
        if (newStatus == null) {
            return;
        }
        var validStatuses = validTransitions.get(chatStatus);
        if (validStatuses == null) {
            throw new ChatTransitionException(chatStatus, newStatus);
        }
        var count = validStatuses.stream()
                .filter(status -> status == newStatus)
                .count();
        if (count == 0) {
            throw new ChatTransitionException(chatStatus, newStatus);
        }
    }

    private void validateIsAnonymous(Boolean isAnonymous, Boolean newIsAnonymous) {
        if (newIsAnonymous == null) {
            return;
        }
        if (!isAnonymous && newIsAnonymous) {
            throw new ChatTransitionException(isAnonymous, newIsAnonymous);
        }
    }

    private void validateDoctor(ChatStatus newStatus, User doctor) {
        if (newStatus == ChatStatus.RESOLVED && doctor == null) {
            throw new ChatTransitionException(null);
        }
        if (doctor == null) {
            return;
        }
        var foundDoctor = userDao.getUserById(doctor.getId());
        if (foundDoctor.getRole() != UserRole.DOCTOR) {
            throw new ChatTransitionException(foundDoctor.getId());
        }
    }

}
