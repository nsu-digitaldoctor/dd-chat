package com.digital.doctor.ddchat.service;

import com.digital.doctor.ddchat.dao.ChatDao;
import com.digital.doctor.ddchat.dao.UserDao;
import com.digital.doctor.ddchat.exception.ChatNotFoundException;
import com.digital.doctor.ddchat.exception.CustomException;
import com.digital.doctor.ddchat.exception.UserChatPermissionException;
import com.digital.doctor.ddchat.exception.UserNotFoundException;
import com.digital.doctor.ddchat.exception.UserRoleException;
import com.digital.doctor.ddchat.model.Chat;
import com.digital.doctor.ddchat.model.Message;
import com.digital.doctor.ddchat.model.User;
import com.digital.doctor.ddchat.model.enums.OperationType;
import com.digital.doctor.ddchat.model.enums.UserRole;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class UserValidatorServiceImpl implements UserValidatorService {

    private final UserDao userDao;

    private final ChatDao chatDao;

    public void validateUserRole(User user, OperationType operationType) {
        final var foundUser = findUser(user);

        if (operationType == OperationType.CREATE_CHAT && foundUser.getRole() != UserRole.USER) {
            throw new UserRoleException(foundUser.getRole(), "Создание обсуждения");
        } else if (operationType == OperationType.GET_USER_CHATS && foundUser.getRole() != UserRole.USER) {
            throw new UserRoleException(foundUser.getRole(), "Получение списка обсуждений пользователя");
        } else if (operationType == OperationType.GET_DOCTOR_CHATS && foundUser.getRole() != UserRole.DOCTOR) {
            throw new UserRoleException(foundUser.getRole(), "Получение списка всех обсуждений");
        }
    }

    @Override
    public void validateUserChatPermissions(User user, Chat chat, OperationType operationType) throws CustomException {
        final var foundUser = findUser(user);
        final var foundChat = findChat(chat);

        if (operationType == OperationType.UPDATE_CHAT) {
            if (foundUser.getRole() != UserRole.USER) {
                throw new UserRoleException(foundUser.getRole(), "Обновление статуса обсуждения");
            } else if (!foundChat.getUser().getId().equals(foundUser.getId())) {
                throw new UserChatPermissionException(foundUser.getId(), foundChat.getId(), "Обновление статуса");
            }
        }
    }

    @Override
    public void validateUserChatStatus(
            User user, Message message, Chat chat, OperationType operationType
    ) throws CustomException {
        final var foundUser = findUser(user);

        if (operationType == OperationType.CREATE_MESSAGE) {
            if (foundUser.getRole().isUser()) {
                if (chat.getStatus().isClosed()) {
                    throw new UserRoleException("Нельзя создать сообщение в закрытом чате");
                } else if (!foundUser.getId().equals(chat.getUser().getId())) {
                    throw new UserRoleException("Нельзя создать сообщение в чужом чате");
                }
            } else if (foundUser.getRole().isDoctor()) {
                if (chat.getStatus().isClosed()) {
                    throw new UserRoleException("Нельзя создать сообщение в закрытом чате");
                }
            }
        } else if (operationType == OperationType.DELETE_MESSAGE) {
            if (chat.getStatus().isClosed()) {
                throw new UserRoleException("Нельзя удалить сообщение в закрытом чате");
            } else if (!foundUser.getId().equals(message.getUser().getId())) {
                throw new UserRoleException("Нельзя удалить чужое сообщение");
            }
        } else if (operationType == OperationType.GET_MESSAGES) {
            if (foundUser.getRole().isUser()) {
                if (chat.getStatus().isClosed()) {
                    throw new UserRoleException("Нельзя получить сообщения в закрытом чате");
                } else if (!foundUser.getId().equals(chat.getUser().getId())) {
                    throw new UserRoleException("Нельзя получать сообщения в чужом чате");
                }
            } else if (foundUser.getRole().isDoctor()) {
                if (chat.getStatus().isClosed()) {
                    throw new UserRoleException("Нельзя получить сообщения в закрытом чате");
                }
            }
        }

        // Потом обновить статус чата если пользователь -- врач.
        user.setRole(foundUser.getRole());
    }

    private User findUser(User user) {
        final User foundUser;
        var userId = user == null
                ? null
                : user.getId();
        try {
            foundUser = userDao.getUserById(userId);
        } catch (Exception e) {
            throw new UserNotFoundException(userId);
        }
        if (foundUser == null) {
            throw new UserNotFoundException(userId);
        }

        if (foundUser.getRole() == null) {
            throw new UserRoleException("Пользователь не имеет роли");
        }

        return foundUser;
    }

    private Chat findChat(Chat chat) {
        final Chat foundChat;
        var chatId = chat == null
                ? null
                : chat.getId();
        try {
            foundChat = chatDao.getChat(chatId);
        } catch (Exception e) {
            throw new ChatNotFoundException(chatId);
        }
        if (foundChat == null) {
            throw new ChatNotFoundException(chatId);
        }

        return foundChat;
    }


}
