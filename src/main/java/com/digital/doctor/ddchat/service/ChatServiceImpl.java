package com.digital.doctor.ddchat.service;

import com.digital.doctor.ddchat.dao.ChatDao;
import com.digital.doctor.ddchat.dao.filter.ChatFilter;
import com.digital.doctor.ddchat.model.Chat;
import com.digital.doctor.ddchat.model.GetChatsInfo;
import com.digital.doctor.ddchat.model.UpdateChatInfo;
import com.digital.doctor.ddchat.model.User;
import com.digital.doctor.ddchat.model.enums.ChatStatus;
import com.digital.doctor.ddchat.model.enums.OperationType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ChatServiceImpl implements ChatService {

    private static final Logger LOGGER = LogManager.getLogger(ChatServiceImpl.class);

    /**
     * Класс для работы проверки возможности совершения операций пользователем.
     */
    private final UserValidatorService userValidatorService;

    /**
     * Класс для работы проверки возможности совершения операций обновления над обсуждением.
     */
    private final ChatTransitionValidatorService chatTransitionValidatorService;

    /**
     * Класс для работы с обсуждениями на слое данных.
     */
    private final ChatDao chatDao;

    @Autowired
    public ChatServiceImpl(
            UserValidatorService userValidatorService,
            ChatTransitionValidatorService chatTransitionValidatorService,
            ChatDao chatDao) {
        this.userValidatorService = userValidatorService;
        this.chatTransitionValidatorService = chatTransitionValidatorService;
        this.chatDao = chatDao;
    }


    @Override
    public Optional<Chat> createChat(Chat chat, User user) {
        try {
            userValidatorService.validateUserRole(user, OperationType.CREATE_CHAT);
            prepareChatForCreation(chat, user);
            var saved = chatDao.save(chat);
            return Optional.ofNullable(saved);
        } catch (Exception exception) {
            LOGGER.error("Ошибка при создании обсуждения: " + exception.getMessage());
            throw exception;
        }
    }

    @Override
    public List<Chat> getUserChats(GetChatsInfo getChatsInfo) {
        try {
            userValidatorService.validateUserRole(getChatsInfo.getUser(), OperationType.GET_USER_CHATS);
            var chatFilter = getChatsInfo.getChatFilter();
            var user = getChatsInfo.getUser();
            var page = getChatsInfo.getPage();
            return chatDao.getChats(
                    ChatFilter.builder()
                            .userId(user.getId())
                            .isAnonymous(chatFilter.getIsAnonymous())
                            .status(chatFilter.getStatus())
                            .pageIndex(page.getId())
                            .pageSize(page.getSize())
                            .build()
            );
        } catch (Exception exception) {
            LOGGER.error("Ошибка поиска пользовательских обсуждений: " + exception.getMessage());
            throw exception;
        }
    }

    @Override
    public List<Chat> getDoctorChats(GetChatsInfo getChatsInfo) {
        try {
            userValidatorService.validateUserRole(getChatsInfo.getUser(), OperationType.GET_DOCTOR_CHATS);
            var chatFilter = getChatsInfo.getChatFilter();
            var page = getChatsInfo.getPage();
            return chatDao.getChats(
                    ChatFilter.builder()
                            .isAnonymous(chatFilter.getIsAnonymous())
                            .status(chatFilter.getStatus())
                            .pageIndex(page.getId())
                            .pageSize(page.getSize())
                            .build()
            );
        } catch (Exception exception) {
            LOGGER.error("Ошибка поиска обсуждений: " + exception.getMessage());
            throw exception;
        }
    }

    @Override
    public List<Chat> updateChats(UpdateChatInfo updateChatInfo) {
        List<Chat> updatedChats = new ArrayList<>();
        for (final var chat : updateChatInfo.getChats()) {
            var updated = updateChat(chat, updateChatInfo.getUser());
            updatedChats.add(updated);
        }
        return updatedChats;
    }

    private Chat updateChat(Chat chat, User user) {
        try {
            userValidatorService.validateUserRole(user, OperationType.UPDATE_CHAT);
            userValidatorService.validateUserChatPermissions(user, chat, OperationType.UPDATE_CHAT);
            chatTransitionValidatorService.validateTransition(chat);
            var foundChat = chatDao.getChat(chat.getId());
            return doUpdate(foundChat, chat);
        } catch (Exception exception) {
            LOGGER.error("Ошибка обновления статусов: " + exception.getMessage());
            throw exception;
        }
    }

    private Chat doUpdate(Chat oldChat, Chat newChat) {
        if (newChat.getStatus() != null) {
            oldChat.setStatus(newChat.getStatus());
            if (newChat.getStatus() == ChatStatus.RESOLVED) {
                oldChat.setDoctor(
                        newChat.getDoctor()
                );
            }
        }
        if (newChat.getIsAnonymous() != null) {
            oldChat.setIsAnonymous(
                    newChat.getIsAnonymous()
            );
        }
        return chatDao.save(oldChat);
    }

    private void prepareChatForCreation(Chat chat, User user) {
        // Нельзя задать идентификатор обсуждения при создании.
        chat.setId(null);
        // Присвоение признака создателя.
        chat.setUser(user);
        // Обсуждение всегда создается в статусе NEW.
        chat.setStatus(ChatStatus.NEW);
    }

}
