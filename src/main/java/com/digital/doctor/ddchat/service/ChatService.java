package com.digital.doctor.ddchat.service;

import com.digital.doctor.ddchat.model.Chat;
import com.digital.doctor.ddchat.model.GetChatsInfo;
import com.digital.doctor.ddchat.model.UpdateChatInfo;
import com.digital.doctor.ddchat.model.User;

import java.util.List;
import java.util.Optional;

/**
 * Сервис по работе с обсуждениями.
 */
public interface ChatService {

    /**
     * Создает обсуждение.
     *
     * @param chat
     *     Обсуждение, которое необходимо создать.
     * @param user
     *     Пользователь, который создает обсуждение.
     * @return
     *     Созданное обсуждение.
     */
    Optional<Chat> createChat(Chat chat, User user);

    /**
     * Возвращает список обсуждений пользователя по запросу.
     *
     * @param getChatsInfo
     *     Запрос на получение списка обсуждений.
     * @return список обсуждений.
     */
    List<Chat> getUserChats(GetChatsInfo getChatsInfo);

    /**
     * Возвращает список обсуждений по запросу.
     *
     * @param getChatsInfo
     *     Запрос на получение списка обсуждений.
     * @return список обсуждений.
     */
    List<Chat> getDoctorChats(GetChatsInfo getChatsInfo);

    /**
     * Обновляет статусы обсуждений по запросу.
     *
     * @param updateChatInfo
     *     Запрос на обновление.
     * @return список обновленных обсуждений.
     */
    List<Chat> updateChats(UpdateChatInfo updateChatInfo);

}
