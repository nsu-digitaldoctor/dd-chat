package com.digital.doctor.ddchat.controller;

import com.digital.doctor.ddchat.dao.converter.ChatDtoConverter;
import com.digital.doctor.ddchat.dao.converter.GetChatsConverter;
import com.digital.doctor.ddchat.dao.converter.UpdateChatsConverter;
import com.digital.doctor.ddchat.dao.converter.UserDtoConverter;
import com.digital.doctor.ddchat.dto.*;
import com.digital.doctor.ddchat.service.ChatService;
import com.digital.doctor.ddchat.validator.JsonSchemaValidator;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.stream.Collectors;

@RestController
@RequestMapping("/chat/v1")
public class ChatController {

    /**
     * Сервис по работе с обсуждениями.
     */
    private final ChatService chatService;

    /**
     * Преобразователь из DTO обсуждения в модель обсуждения.
     */
    private final ChatDtoConverter chatDtoConverter;

    /**
     * Преобразователь из DTO пользователя в модель пользователя.
     */
    private final UserDtoConverter userDtoConverter;

    /**
     * Преобразователь запроса списка обсуждений в модель запроса.
     */
    private final GetChatsConverter getChatsConverter;

    /**
     * Преобразователь запроса обновления обсуждений в модель запроса.
     */
    private final UpdateChatsConverter updateChatsConverter;

    /**
     * Класс для валидации входных параметров.
     */
    private final JsonSchemaValidator chatValidator;

    @Autowired
    public ChatController(
            ChatService chatService,
            ChatDtoConverter chatDtoConverter,
            UserDtoConverter userDtoConverter,
            GetChatsConverter getChatsConverter,
            UpdateChatsConverter updateChatsConverter,
            @Qualifier("chatValidator")
            JsonSchemaValidator chatValidator) {
        this.chatService = chatService;
        this.chatDtoConverter = chatDtoConverter;
        this.userDtoConverter = userDtoConverter;
        this.getChatsConverter = getChatsConverter;
        this.updateChatsConverter = updateChatsConverter;
        this.chatValidator = chatValidator;
    }

    @PostMapping("createChat")
    public ResponseEntity<ChatDto> createChat(
            @RequestBody
            CreateChatDto createChatDto) {
        chatValidator.validate(createChatDto);
        var chat = chatDtoConverter.toModel(createChatDto.getChat());
        var user = userDtoConverter.toModel(createChatDto.getUser());
        var saved = chatService.createChat(chat, user);
        return ResponseEntity.of(
                saved.map(
                        chatDtoConverter::toDto
                )
        );
    }

    @PostMapping("getUserChatsList")
    public ChatListDto getUserChats(
            @RequestBody
            GetChatListDto getChatListDto) {
        chatValidator.validate(getChatListDto);
        var getChatsInfo = getChatsConverter.toModel(getChatListDto);
        return ChatListDto.builder()
                .chats(
                        chatService.getUserChats(getChatsInfo).stream()
                                .map(chatDtoConverter::toDto)
                                .collect(Collectors.toList())
                )
                .build();
    }

    @PostMapping("getChatsList")
    public ChatListDto getChatsList(
            @RequestBody
            GetChatListDto getChatListDto) {
        chatValidator.validate(getChatListDto);
        var getChatsInfo = getChatsConverter.toModel(getChatListDto);
        return ChatListDto.builder()
                .chats(
                        chatService.getDoctorChats(getChatsInfo).stream()
                                .map(chatDtoConverter::toDto)
                                .collect(Collectors.toList())
                )
                .build();
    }

    @PostMapping("updateChat")
    public ChatListDto updateChats(
            @RequestBody
            UpdateChatDto updateChatDto) {
        chatValidator.validate(updateChatDto);
        var updateChatInfo = updateChatsConverter.toModel(updateChatDto);
        return ChatListDto.builder()
                .chats(
                        chatService.updateChats(updateChatInfo).stream()
                                .map(chatDtoConverter::toDto)
                                .collect(Collectors.toList())
                )
                .build();
    }

    @ExceptionHandler({Exception.class})
    StatusDto handleValidationException(HttpServletRequest request, Exception exception) {
        return StatusDto.builder()
                .error(exception.getClass().getName())
                .description(exception.getMessage())
                .build();
    }

}
