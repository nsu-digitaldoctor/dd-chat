package com.digital.doctor.ddchat.controller;

import com.digital.doctor.ddchat.dao.converter.ChatDtoConverter;
import com.digital.doctor.ddchat.dao.converter.MessageDtoConverter;
import com.digital.doctor.ddchat.dao.converter.UserDtoConverter;
import com.digital.doctor.ddchat.dto.CreateMessageDto;
import com.digital.doctor.ddchat.dto.DeleteMessageDto;
import com.digital.doctor.ddchat.dto.GetMessageListDto;
import com.digital.doctor.ddchat.dto.MessageDto;
import com.digital.doctor.ddchat.dto.MessageListDto;
import com.digital.doctor.ddchat.dto.StatusDto;
import com.digital.doctor.ddchat.service.MessageService;
import com.digital.doctor.ddchat.validator.JsonSchemaValidator;
import jakarta.servlet.http.HttpServletRequest;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.stream.Collectors;

@AllArgsConstructor
@RestController
@RequestMapping("/chat/v1")
public class MessageController {

    private final MessageService messageService;
    @Qualifier("messageValidator")
    private final JsonSchemaValidator messageValidator;
    private final MessageDtoConverter messageDtoConverter;
    private final UserDtoConverter userDtoConverter;
    private final ChatDtoConverter chatDtoConverter;

    @PostMapping("/createMessage")
    ResponseEntity<MessageDto> createMessage(@RequestBody CreateMessageDto createMessageDto) {
        messageValidator.validate(createMessageDto);

        var message = messageDtoConverter.toModel(createMessageDto.getMessage());
        var user = userDtoConverter.toModel(createMessageDto.getUser());
        var saved = messageService.createMessage(message, user);

        return ResponseEntity.of(saved.map(messageDtoConverter::toDto));
    }

    @DeleteMapping("/deleteMessage")
    ResponseEntity<String> deleteMessage(@RequestBody DeleteMessageDto deleteMessageDto) {
        messageValidator.validate(deleteMessageDto);

        var message = messageDtoConverter.toModel(deleteMessageDto.getMessage());
        var user = userDtoConverter.toModel(deleteMessageDto.getUser());

        messageService.deleteMessage(message, user);

        return ResponseEntity.ok("ok");
    }

    @ExceptionHandler({Exception.class})
    StatusDto handleValidationException(HttpServletRequest request, Exception exception) {
        return StatusDto.builder()
                .error(exception.getClass().getName())
                .description(exception.getMessage())
                .build();
    }

    @PostMapping("getMessagesList")
    public MessageListDto getMessagesList(@RequestBody GetMessageListDto getMessageListDto) {
        messageValidator.validate(getMessageListDto);

        var chat = chatDtoConverter.toModel(getMessageListDto.getChat());
        var user = userDtoConverter.toModel(getMessageListDto.getUser());
        var messages = messageService.getMessages(chat, user);

        return new MessageListDto(
                messages.stream().map(messageDtoConverter::toDto).collect(Collectors.toList())
        );
    }
}
