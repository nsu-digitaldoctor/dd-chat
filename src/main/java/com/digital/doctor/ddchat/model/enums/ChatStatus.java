package com.digital.doctor.ddchat.model.enums;

/**
 * Статус (состояние) обсуждения.
 */
public enum ChatStatus {

    NEW,
    ANSWERED,
    RESOLVED,
    CLOSED;

    public boolean isClosed() {
        return this == CLOSED;
    }

}
