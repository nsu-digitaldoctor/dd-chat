package com.digital.doctor.ddchat.model;

import com.digital.doctor.ddchat.model.enums.ChatStatus;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ChatFilter {

    ChatStatus status;

    Boolean isAnonymous;

}
