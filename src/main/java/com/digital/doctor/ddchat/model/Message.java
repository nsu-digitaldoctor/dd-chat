package com.digital.doctor.ddchat.model;

import lombok.*;

import java.time.OffsetDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Message {

    private Integer id;

    private User user;

    private Chat chat;

    private String message;

    private OffsetDateTime sentDate;

    private boolean isDeleted;

    private Message replyMessage;

}