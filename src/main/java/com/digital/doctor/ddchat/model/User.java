package com.digital.doctor.ddchat.model;

import com.digital.doctor.ddchat.model.enums.UserRole;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Сущность пользователя.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class User {

    /**
     * Идентификатор в системе хранения.
     */
    private Integer id;

    private String firstName;

    private String middleName;

    private String lastName;

    /**
     * Роль пользователя в системе.
     */
    private UserRole role;

}
