package com.digital.doctor.ddchat.model;

import com.digital.doctor.ddchat.model.enums.ChatStatus;
import lombok.*;

import java.time.OffsetDateTime;


/**
 * Сущность обсуждения.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Chat {

    /**
     * Идентификатор в системе хранения.
     */
    private Integer id;

    /**
     * Пользователь создавший обсуждение.
     */
    private User user;

    /**
     * Название обсуждения.
     */
    private String title;

    /**
     * Признак является ли обсуждение анонимным.
     */
    private Boolean isAnonymous;

    /**
     * Статус обсуждения.
     */
    private ChatStatus status;

    /**
     * Доктор решивший обсуждение.
     */
    private User doctor;

    /**
     * Дата и время создания обсуждения.
     */
    private OffsetDateTime createDate;

}
