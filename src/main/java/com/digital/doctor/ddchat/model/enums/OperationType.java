package com.digital.doctor.ddchat.model.enums;

public enum OperationType {

    CREATE_CHAT,

    GET_USER_CHATS,

    GET_DOCTOR_CHATS,

    UPDATE_CHAT,

    CREATE_MESSAGE,

    DELETE_MESSAGE,

    GET_MESSAGES

}
