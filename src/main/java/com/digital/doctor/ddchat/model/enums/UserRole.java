package com.digital.doctor.ddchat.model.enums;

/**
 * Роль пользователя в системе.
 */
public enum UserRole {

    USER,
    DOCTOR;

    public boolean isUser() {
        return this == USER;
    }

    public boolean isDoctor() {
        return this == DOCTOR;
    }

}
