package com.digital.doctor.ddchat.validator;

import com.digital.doctor.ddchat.exception.ValidationException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.networknt.schema.JsonSchema;
import com.networknt.schema.JsonSchemaFactory;
import com.networknt.schema.SpecVersion;
import com.networknt.schema.ValidationMessage;

import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Класс для валидации dto по схеме.
 */
public class JsonSchemaValidator {

    private final JsonSchemaFactory jsonSchemaFactory = JsonSchemaFactory.getInstance(SpecVersion.VersionFlag.V4);

    private final Map<Class<?>, JsonSchema> schemas;

    private final ObjectMapper objectMapper;

    public JsonSchemaValidator(Map<Class<?>, String> schemas, ObjectMapper objectMapper) {
        this.schemas = readSchemas(schemas);
        this.objectMapper = objectMapper;
    }

    private Map<Class<?>, JsonSchema> readSchemas(Map<Class<?>, String> schemas) {
        return schemas.entrySet().stream()
                .map(this::readSchema)
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue
                ));
    }

    private Map.Entry<Class<?>, JsonSchema> readSchema(Map.Entry<Class<?>, String> schemaPath) {
        JsonSchema jsonSchema = jsonSchemaFactory.getSchema(
                this.getClass().getClassLoader().getResourceAsStream(schemaPath.getValue())
        );
        jsonSchema.initializeValidators();
        return Map.entry(schemaPath.getKey(), jsonSchema);
    }

    public void validate(Object obj) {
        final Set<ValidationMessage> errors;
        try {
            var json = objectMapper.valueToTree(obj);
            var schema = schemas.get(obj.getClass());
            errors = schema.validate(json);
        } catch (Exception exception) {
            throw new ValidationException(exception);
        }
        if (!errors.isEmpty()) {
            throw new ValidationException(errors);
        }
    }

}
