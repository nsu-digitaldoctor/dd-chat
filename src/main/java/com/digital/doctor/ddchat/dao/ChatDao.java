package com.digital.doctor.ddchat.dao;

import com.digital.doctor.ddchat.dao.filter.ChatFilter;
import com.digital.doctor.ddchat.model.Chat;

import java.util.List;

/**
 * Класс для работы с обсуждениями на слое данных.
 */
public interface ChatDao {

    Chat getChat(Integer chatId);

    /**
     * Возвращает список обсуждений подходящих по фильтру.
     *
     * @param filter Фильтр.
     * @return Список обсуждений подходящих по фильтру.
     */
    List<Chat> getChats(ChatFilter filter);

    /**
     * Сохраняет обсуждение.
     *
     * @param chat
     *     Модель обсуждения.
     *
     * @return Модель сохраненного обсуждения.
     */
    Chat save(Chat chat);

    /**
     * Удаляет обсуждение.
     *
     * @param chatId
     *     Идентификатор обсуждения.
     */
    void delete(Integer chatId);

}
