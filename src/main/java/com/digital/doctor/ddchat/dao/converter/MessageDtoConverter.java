package com.digital.doctor.ddchat.dao.converter;

import com.digital.doctor.ddchat.dao.ChatDao;
import com.digital.doctor.ddchat.dao.MessageDao;
import com.digital.doctor.ddchat.dto.MessageDto;
import com.digital.doctor.ddchat.dto.UserDto;
import com.digital.doctor.ddchat.model.Message;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.time.OffsetDateTime;

@Component
@AllArgsConstructor
public class MessageDtoConverter implements DtoToModelConverter<MessageDto, Message> {

    private final MessageDao messageDao;
    private final UserDtoConverter userConverter;
    private final ChatDao chatDao;

    @Override
    public MessageDto toDto(Message model) {
        return MessageDto.builder()
                .messageId(model.getId())
                .content(model.getMessage())
                .sentDate(model.getSentDate())
                .replyMessageId(model.getReplyMessage().getId())
                .user(getMessageUser(model))
                .chatId(model.getChat().getId())
                .build();
    }

    @Override
    public Message toModel(MessageDto dto) {
        var message = messageDao.getMessageById(dto.getMessageId());
        if (message != null) {
            return message;
        }

        return Message.builder()
                .id(dto.getMessageId())
                .message(dto.getContent())
                .chat(chatDao.getChat(dto.getChatId()))
                .sentDate(OffsetDateTime.now())
                .replyMessage(messageDao.getMessageById(dto.getReplyMessageId()))
                .build();
    }

    private UserDto getMessageUser(Message message) {
        var isAnonymous = chatDao.getChat(message.getChat().getId()).getIsAnonymous();
        var user = message.getUser();

        if (user.getRole().isUser() && isAnonymous) {
            return null;
        }

        return userConverter.toDto(user);
    }
}
