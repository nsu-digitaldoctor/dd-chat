package com.digital.doctor.ddchat.dao.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.OffsetDateTime;

@Entity
@Table(name = "message")
@Getter
@Setter
@NoArgsConstructor
public class MessageEntity extends BaseEntity {

    /**
     * Идентификатор пользователя, который создал обсуждение.
     */
    @Column(name = "user_id", nullable = false)
    private Integer userId;

    /**
     * Идентификатор обсуждения.
     */
    @Column(name = "thread_id", nullable = false)
    private Integer threadId;

    /**
     * Текст сообщения.
     */
    @Column(name = "message", nullable = false)
    private String message;

    /**
     * Дата и время отправки сообщения
     */
    @Column(name = "sent_date", nullable = false)
    private OffsetDateTime sentDate;

    /**
     * Признак того, что сообщение удалено
     */
    @Column(name = "is_deleted", nullable = false)
    private boolean isDeleted;

    /**
     * Идентифиактор сообщения, на которое осуществлен ответ
     */
    @Column(name = "reply_message_id", nullable = true)
    private Integer replyMessageId;

    @Builder
    public MessageEntity(
            Integer id,
            Integer userId,
            Integer threadId,
            String message,
            OffsetDateTime sentDate,
            boolean isDeleted,
            Integer replyMessageId) {
        super(id);
        this.userId = userId;
        this.threadId = threadId;
        this.message = message;
        this.sentDate = sentDate;
        this.isDeleted = isDeleted;
        this.replyMessageId = replyMessageId;
    }
}
