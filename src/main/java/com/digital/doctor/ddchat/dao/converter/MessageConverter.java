package com.digital.doctor.ddchat.dao.converter;

import com.digital.doctor.ddchat.dao.ChatDao;
import com.digital.doctor.ddchat.dao.UserDao;
import com.digital.doctor.ddchat.dao.entity.MessageEntity;
import com.digital.doctor.ddchat.model.Message;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@AllArgsConstructor
@Component
public class MessageConverter implements EntityToModelConverter<MessageEntity, Message> {

    private final ChatDao chatDao;
    private final UserDao userDao;

    @Override
    public MessageEntity toEntity(Message model) {
        if (model == null) {
            return null;
        }

        return MessageEntity.builder()
                .id(model.getId())
                .userId(model.getUser().getId())
                .threadId(model.getChat().getId())
                .message(model.getMessage())
                .sentDate(model.getSentDate())
                .replyMessageId(
                        Optional.ofNullable(model.getReplyMessage()).map(Message::getId).orElse(null)
                )
                .build();
    }

    @Override
    public Message toModel(MessageEntity entity) {
        if (entity == null) {
            return null;
        }

        return Message.builder()
                .id(entity.getId())
                .user(userDao.getUserById(entity.getUserId()))
                .chat(chatDao.getChat(entity.getThreadId()))
                .message(entity.getMessage())
                .sentDate(entity.getSentDate())
                .isDeleted(entity.isDeleted())
                .replyMessage(convertReplyMessage(entity.getReplyMessageId()))
                .build();
    }

    private Message convertReplyMessage(Integer replyMessageId) {
        return new Message(replyMessageId, null, null, null, null, false, null);
    }
}
