package com.digital.doctor.ddchat.dao;

import com.digital.doctor.ddchat.dao.converter.MessageConverter;
import com.digital.doctor.ddchat.dao.entity.MessageEntity;
import com.digital.doctor.ddchat.dao.exception.DaoException;
import com.digital.doctor.ddchat.dao.filter.MessageFilter;
import com.digital.doctor.ddchat.dao.jpa.chat.MessageJpaRepository;
import com.digital.doctor.ddchat.model.Message;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Component
@AllArgsConstructor
public class MessageDaoImpl implements MessageDao {

    private final MessageJpaRepository messageJpaRepository;
    private final MessageConverter messageConverter;

    @Override
    public List<Message> getMessages(MessageFilter messageFilter) {
        var entities = getMessageEntities(messageFilter);

        if (entities == null) {
            return Collections.emptyList();
        }

        return entities.stream()
                .filter(it -> !it.isDeleted())
                .map(messageConverter::toModel)
                .collect(Collectors.toList());
    }

    @Override
    public Message getMessageById(Integer messageId) {
        if (messageId == null) {
            return null;
        }

        return messageJpaRepository.findById(messageId).map(messageConverter::toModel).orElse(null);
    }

    @Override
    public Message save(Message message) {
        var entity = messageConverter.toEntity(message);
        return messageConverter.toModel(messageJpaRepository.saveAndFlush(entity));
    }

    @Override
    public void delete(Integer messageId) {
        var entity = messageJpaRepository.findById(messageId).orElse(null);

        if (entity == null) {
            return;
        }

        entity.setDeleted(true);
        messageJpaRepository.saveAndFlush(entity);
    }

    private List<MessageEntity> getMessageEntities(MessageFilter filter) {
        List<MessageEntity> initialList;

        if (filter.getMessageId() != null) {
            initialList = messageJpaRepository.findById(filter.getChatId())
                    .map(Collections::singletonList)
                    .orElse(Collections.emptyList());
        } else if (filter.getChatId() != null) {
            initialList = messageJpaRepository.findByThreadId(filter.getChatId());
        } else if (filter.getUserId() != null) {
            initialList = messageJpaRepository.findByUserId(filter.getUserId());
        } else if (filter.getStartTime() != null) {
            initialList = messageJpaRepository.findBySentDateAfter(filter.getStartTime());
        } else if (filter.getEndTime() != null) {
            initialList = messageJpaRepository.findBySentDateAfter(filter.getEndTime());
        } else {
            throw new DaoException("Фильтр для обсуждений не может быть пустым");
        }

        if (filter.getChatId() != null) {
            return initialList;
        } else {
            return initialList.stream()
                    .filter(filter::isCorrect)
                    .collect(Collectors.toList());
        }
    }
}
