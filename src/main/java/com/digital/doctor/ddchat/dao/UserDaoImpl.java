package com.digital.doctor.ddchat.dao;

import com.digital.doctor.ddchat.dao.converter.UserConverter;
import com.digital.doctor.ddchat.dao.jpa.user.UserJpaRepository;
import com.digital.doctor.ddchat.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserDaoImpl implements UserDao {

    private final UserJpaRepository userJpaRepository;

    private final UserConverter userConverter;

    @Autowired
    public UserDaoImpl(
            UserJpaRepository userJpaRepository,
            UserConverter userConverter) {
        this.userJpaRepository = userJpaRepository;
        this.userConverter = userConverter;
    }

    @Override
    public User getUserById(Integer userId) {
        if (userId == null) {
            return null;
        }
        var user = userJpaRepository.findById(userId);
        return user.map(userConverter::toModel)
                .orElse(null);
    }

}
