package com.digital.doctor.ddchat.dao.converter;

import com.digital.doctor.ddchat.dto.UserDto;
import com.digital.doctor.ddchat.model.User;
import com.digital.doctor.ddchat.model.enums.UserRole;
import org.springframework.stereotype.Component;

@Component
public class UserDtoConverter implements DtoToModelConverter<UserDto, User> {
    @Override
    public UserDto toDto(User model) {
        if (model == null) {
            return null;
        }
        return UserDto.builder()
                .id(model.getId())
                .firstName(model.getFirstName())
                .middleName(model.getMiddleName())
                .lastName(model.getLastName())
                .role(convertRole(model.getRole()))
                .build();
    }

    @Override
    public User toModel(UserDto dto) {
        if (dto == null) {
            return null;
        }
        return User.builder()
                .id(dto.getId())
                .role(convertRole(dto.getRole()))
                .build();
    }

    private String convertRole(UserRole role) {
        if (role == null) {
            return null;
        }
        return role.name();
    }

    private UserRole convertRole(String role) {
        if (role == null) {
            return null;
        }
        return UserRole.valueOf(role);
    }

}
