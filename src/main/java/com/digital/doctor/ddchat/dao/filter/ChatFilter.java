package com.digital.doctor.ddchat.dao.filter;

import com.digital.doctor.ddchat.dao.entity.ChatEntity;
import com.digital.doctor.ddchat.model.enums.ChatStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
@AllArgsConstructor
public class ChatFilter {

    /**
     * Идентификатор обсуждения.
     */
    private final Integer chatId;

    /**
     * Идентификатор пользователя.
     */
    private final Integer userId;

    /**
     * Название обсуждения.
     */
    private final ChatStatus status;

    /**
     * Признак является ли обсуждение анонимным.
     */
    private final Boolean isAnonymous;

    /**
     * Размер страницы.
     */
    private final Integer pageSize;

    /**
     * Индекс страницы.
     */
    private final Integer pageIndex;

    /**
     * Проверка соответствия сущности фильтру.
     *
     * @param entity
     *     Сущность для сверки.
     * @return true при соответствии, иначе false.
     */
    public boolean isCorrect(ChatEntity entity) {
        if (chatId != null && !chatId.equals(entity.getId())) {
            return false;
        }
        if (userId != null && !userId.equals(entity.getUserId())) {
            return false;
        }
        if (status != null && !status.name().equals(entity.getStatus())) {
            return false;
        }
        if (isAnonymous != null && !isAnonymous.equals(entity.getIsAnonymous())) {
            return false;
        }
        return true;
    }

    /**
     * Проверка корректности фильтра.
     *
     * @return true если фильтр корректный, иначе false.
     */
    public boolean correctFilter() {
        // Обязательные параметры.
        if (pageIndex == null || pageSize == null) {
            return false;
        }
        // Индекс натуральное неотрицательное число,
        // размер страницы натуральное положительное число.
        if (pageIndex < 0 || pageSize <= 0) {
            return false;
        }
        return true;
    }

}
