package com.digital.doctor.ddchat.dao.converter;

import com.digital.doctor.ddchat.dto.ChatDto;
import com.digital.doctor.ddchat.dto.UserDto;
import com.digital.doctor.ddchat.model.Chat;
import com.digital.doctor.ddchat.model.enums.ChatStatus;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class ChatDtoConverter implements DtoToModelConverter<ChatDto, Chat> {

    private final UserDtoConverter userConverter;

    @Override
    public ChatDto toDto(Chat model) {
        return ChatDto.builder()
                .id(model.getId())
                .isAnonymous(model.getIsAnonymous())
                .user(getChatUser(model))
                .title(model.getTitle())
                .status(convertStatus(model.getStatus()))
                .createDate(model.getCreateDate())
                .build();
    }

    @Override
    public Chat toModel(ChatDto dto) {
        return Chat.builder()
                .id(dto.getId())
                .isAnonymous(dto.getIsAnonymous())
                .user(userConverter.toModel(dto.getUser()))
                .title(dto.getTitle())
                .status(convertStatus(dto.getStatus()))
                .build();
    }

    private String convertStatus(ChatStatus status) {
        if (status == null) {
            return null;
        }
        return status.name();
    }

    private ChatStatus convertStatus(String status) {
        if (status == null) {
            return null;
        }
        return ChatStatus.valueOf(status);
    }

    private UserDto getChatUser(Chat chat) {
        var isAnonymous = chat.getIsAnonymous();

        if (isAnonymous) {
            return null;
        }

        return userConverter.toDto(chat.getUser());
    }

}
