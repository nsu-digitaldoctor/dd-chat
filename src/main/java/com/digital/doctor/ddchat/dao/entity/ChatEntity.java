package com.digital.doctor.ddchat.dao.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.*;

import java.time.OffsetDateTime;

@Entity
@Table(name = "chat")
@Getter
@Setter
@NoArgsConstructor
public class ChatEntity extends BaseEntity {

    /**
     * Идентификатор пользователя, который создал обсуждение.
     */
    @Column(name = "user_id", nullable = false)
    private Integer userId;

    /**
     * Наименование обсуждения.
     */
    @Column(name = "title", nullable = false)
    private String title;

    /**
     * Признак является ли обсуждение анонимным.
     */
    @Column(name = "is_anonymous", nullable = false)
    private Boolean isAnonymous;

    /**
     * Статус обсуждения.
     */
    @Column(name = "status", nullable = false)
    private String status;

    /**
     * Идентификатор врача, который решил обсуждение.
     */
    @Column(name = "doctor_id", nullable = true)
    private Integer doctorId;

    /**
     * Дата и время создания обсуждения.
     */
    @Column(name = "create_date", nullable = false)
    private OffsetDateTime createDate;

    @Builder
    public ChatEntity(
            Integer id,
            Integer userId,
            String title,
            Boolean isAnonymous,
            String status,
            Integer doctorId,
            OffsetDateTime createDate) {
        super(id);
        this.userId = userId;
        this.title = title;
        this.isAnonymous = isAnonymous;
        this.status = status;
        this.doctorId = doctorId;
        this.createDate = createDate;
    }

}
