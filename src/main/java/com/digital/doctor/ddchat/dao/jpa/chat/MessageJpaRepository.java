package com.digital.doctor.ddchat.dao.jpa.chat;

import com.digital.doctor.ddchat.dao.entity.MessageEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.OffsetDateTime;
import java.util.List;

public interface MessageJpaRepository extends JpaRepository<MessageEntity, Integer> {

    List<MessageEntity> findByUserId(Integer userId);

    List<MessageEntity> findByThreadId(Integer threadId);

    List<MessageEntity> findBySentDateAfter(OffsetDateTime dateTime);

    List<MessageEntity> findBySentDateBefore(OffsetDateTime dateTime);
}
