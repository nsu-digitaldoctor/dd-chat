package com.digital.doctor.ddchat.dao.converter;

import com.digital.doctor.ddchat.dto.ChatFilterDto;
import com.digital.doctor.ddchat.dto.GetChatListDto;
import com.digital.doctor.ddchat.dto.PageDto;
import com.digital.doctor.ddchat.dto.UserDto;
import com.digital.doctor.ddchat.model.ChatFilter;
import com.digital.doctor.ddchat.model.GetChatsInfo;
import com.digital.doctor.ddchat.model.Page;
import com.digital.doctor.ddchat.model.User;
import com.digital.doctor.ddchat.model.enums.ChatStatus;
import org.springframework.stereotype.Component;

@Component
public class GetChatsConverter {

    public GetChatsInfo toModel(GetChatListDto dto) {
        if (dto == null) {
            return null;
        }
        return GetChatsInfo.builder()
                .page(convertPage(dto.getPage()))
                .chatFilter(convertFilter(dto.getFilter()))
                .user(convertUser(dto.getUser()))
                .build();
    }

    private User convertUser(UserDto userDto) {
        if (userDto == null) {
            return null;
        }
        return User.builder()
                .id(userDto.getId())
                .build();
    }

    private ChatFilter convertFilter(ChatFilterDto chatFilterDto) {
        if (chatFilterDto == null) {
            return null;
        }
        return ChatFilter.builder()
                .status(convertStatus(chatFilterDto.getStatus()))
                .isAnonymous(chatFilterDto.getIsAnonymous())
                .build();
    }

    private Page convertPage(PageDto pageDto) {
        if (pageDto == null) {
            return null;
        }
        return Page.builder()
                .id(pageDto.getId())
                .size(pageDto.getSize())
                .build();
    }

    private ChatStatus convertStatus(String chatStatus) {
        if (chatStatus == null) {
            return null;
        }
        try {
            return ChatStatus.valueOf(chatStatus);
        } catch (Exception exception) {
            throw new IllegalStateException("Указано неверное название статуса обсуждения.");
        }
    }

}
