package com.digital.doctor.ddchat.dao;

import com.digital.doctor.ddchat.dao.filter.MessageFilter;
import com.digital.doctor.ddchat.model.Message;

import java.util.List;

public interface MessageDao {

    List<Message> getMessages(MessageFilter messageFilter);

    Message getMessageById(Integer messageId);

    Message save(Message message);

    void delete(Integer messageId);


}
