package com.digital.doctor.ddchat.dao;

import com.digital.doctor.ddchat.model.User;

/**
 * Класс для работы с пользователями на слое данных.
 */
public interface UserDao {

    /**
     * Возвращает пользователя по его идентификатору.
     *
     * @param userId
     *     Идентификатор пользователя.
     * @return Пользователь.
     */
    User getUserById(Integer userId);

}
