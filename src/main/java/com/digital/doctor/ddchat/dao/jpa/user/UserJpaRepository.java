package com.digital.doctor.ddchat.dao.jpa.user;

import com.digital.doctor.ddchat.dao.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserJpaRepository extends JpaRepository<UserEntity, Integer> {}
