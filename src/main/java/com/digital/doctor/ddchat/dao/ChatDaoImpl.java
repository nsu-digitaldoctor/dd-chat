package com.digital.doctor.ddchat.dao;

import com.digital.doctor.ddchat.dao.converter.ChatConverter;
import com.digital.doctor.ddchat.dao.entity.ChatEntity;
import com.digital.doctor.ddchat.dao.exception.DaoException;
import com.digital.doctor.ddchat.dao.filter.ChatFilter;
import com.digital.doctor.ddchat.dao.jpa.chat.ChatJpaRepository;
import com.digital.doctor.ddchat.dao.jpa.chat.ChatPaginationRepository;
import com.digital.doctor.ddchat.model.Chat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class ChatDaoImpl implements ChatDao {

    private final ChatJpaRepository chatJpaRepository;

    private final ChatPaginationRepository chatPaginationRepository;

    private final ChatConverter converter;

    @Autowired
    public ChatDaoImpl(
            ChatJpaRepository chatJpaRepository,
            ChatPaginationRepository chatPaginationRepository,
            ChatConverter converter) {
        this.chatJpaRepository = chatJpaRepository;
        this.chatPaginationRepository = chatPaginationRepository;
        this.converter = converter;
    }

    @Override
    public Chat getChat(Integer chatId) {
        return chatJpaRepository.findById(chatId).map(converter::toModel).orElse(null);
    }

    @Override
    public List<Chat> getChats(ChatFilter filter) {
        var entities = getChatsPaginated(filter);
        if (entities == null) {
            return null;
        }
        return entities.stream()
                .map(converter::toModel)
                .collect(Collectors.toList());
    }

    @Override
    public Chat save(Chat chat) {
        if (chat.getId() == null) {
            chat.setCreateDate(OffsetDateTime.now());
        }
        var entity = converter.toEntity(chat);
        return converter.toModel(
                chatJpaRepository.saveAndFlush(entity)
        );
    }

    @Override
    public void delete(Integer chatId) {
        chatJpaRepository.deleteById(chatId);
    }

    private List<ChatEntity> getChatsPaginated(ChatFilter filter) {
        if (!filter.correctFilter()) {
            throw new DaoException("Указан некорректный фильтр поиска.");
        }
        Pageable pageable = PageRequest.of(
                filter.getPageIndex(),
                filter.getPageSize()
        );
        if (filter.getUserId() != null) {
            var userId = filter.getUserId();
            if (filter.getStatus() != null && filter.getIsAnonymous() != null) {
                return chatPaginationRepository.findAllByUserIdAndIsAnonymousAndStatus(
                        userId,
                        filter.getIsAnonymous(),
                        filter.getStatus().name(),
                        pageable
                );
            } else if (filter.getStatus() != null) {
                return chatPaginationRepository.findAllByUserIdAndStatus(
                        userId,
                        filter.getStatus().name(),
                        pageable
                );
            } else if (filter.getIsAnonymous() != null) {
                return chatPaginationRepository.findAllByUserIdAndIsAnonymous(
                        userId,
                        filter.getIsAnonymous(),
                        pageable
                );
            } else {
                return chatPaginationRepository.findAllByUserId(userId, pageable);
            }
        } else {
            if (filter.getStatus() != null && filter.getIsAnonymous() != null) {
                return chatPaginationRepository.findAllByIsAnonymousAndStatus(
                        filter.getIsAnonymous(),
                        filter.getStatus().name(),
                        pageable
                );
            } else if (filter.getStatus() != null) {
                return chatPaginationRepository.findAllByStatus(
                        filter.getStatus().name(),
                        pageable
                );
            } else if (filter.getIsAnonymous() != null) {
                return chatPaginationRepository.findAllByIsAnonymous(
                        filter.getIsAnonymous(),
                        pageable
                );
            } else {
                return chatPaginationRepository.findAll(pageable).stream().toList();
            }
        }
    }

}
