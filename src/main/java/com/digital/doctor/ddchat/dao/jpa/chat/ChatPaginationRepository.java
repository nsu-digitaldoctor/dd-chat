package com.digital.doctor.ddchat.dao.jpa.chat;

import com.digital.doctor.ddchat.dao.entity.ChatEntity;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ChatPaginationRepository extends PagingAndSortingRepository<ChatEntity, Integer> {

    List<ChatEntity> findAllByUserId(Integer userId, Pageable pageable);

    List<ChatEntity> findAllByUserIdAndStatus(Integer userId, String status, Pageable pageable);

    List<ChatEntity> findAllByUserIdAndIsAnonymous(Integer userId, Boolean isAnonymous, Pageable pageable);

    List<ChatEntity> findAllByUserIdAndIsAnonymousAndStatus(
            Integer userId,
            Boolean isAnonymous,
            String status,
            Pageable pageable
    );

    List<ChatEntity> findAllByStatus(String status, Pageable pageable);
    List<ChatEntity> findAllByIsAnonymous(Boolean isAnonymous, Pageable pageable);

    List<ChatEntity> findAllByIsAnonymousAndStatus(
            Boolean isAnonymous,
            String status,
            Pageable pageable
    );

}
