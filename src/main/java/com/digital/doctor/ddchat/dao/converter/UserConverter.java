package com.digital.doctor.ddchat.dao.converter;

import com.digital.doctor.ddchat.dao.entity.UserEntity;
import com.digital.doctor.ddchat.model.User;
import com.digital.doctor.ddchat.model.enums.UserRole;
import org.springframework.stereotype.Component;

@Component
public class UserConverter implements EntityToModelConverter<UserEntity, User> {

    @Override
    public UserEntity toEntity(User model) {
        if (model == null) {
            return null;
        }
        return UserEntity.builder()
                .id(model.getId())
                .role(model.getRole().name())
                .build();
    }

    @Override
    public User toModel(UserEntity entity) {
        if (entity == null) {
            return null;
        }
        return new User(
                entity.getId(),
                entity.getFirstName(),
                entity.getMiddleName(),
                entity.getLastName(),
                UserRole.valueOf(entity.getRole())
        );
    }

    private String convertRole(UserRole role) {
        if (role == null) {
            return null;
        }
        return role.name();
    }

    private UserRole convertRole(String role) {
        if (role == null) {
            return null;
        }
        return UserRole.valueOf(role);
    }

}
