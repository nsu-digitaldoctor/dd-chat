package com.digital.doctor.ddchat.dao.exception;

/**
 * Исключение при обращении к слою данных.
 */
public class DaoException extends RuntimeException {

    public DaoException(String message) {
        super(message);
    }

}
