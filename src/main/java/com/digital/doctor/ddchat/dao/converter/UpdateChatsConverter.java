package com.digital.doctor.ddchat.dao.converter;

import com.digital.doctor.ddchat.dto.ChatStatusChangeDto;
import com.digital.doctor.ddchat.dto.UpdateChatDto;
import com.digital.doctor.ddchat.dto.UserDto;
import com.digital.doctor.ddchat.model.Chat;
import com.digital.doctor.ddchat.model.UpdateChatInfo;
import com.digital.doctor.ddchat.model.User;
import com.digital.doctor.ddchat.model.enums.ChatStatus;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Преобразователь запроса обновления обсуждений в модель запроса.
 */
@Component
public class UpdateChatsConverter {

    public UpdateChatInfo toModel(UpdateChatDto updateChatDto) {
        if (updateChatDto == null) {
            return null;
        }
        return UpdateChatInfo.builder()
                .chats(convertChats(updateChatDto.getChats()))
                .user(convertUser(updateChatDto.getUser()))
                .build();
    }

    private List<Chat> convertChats(List<ChatStatusChangeDto> changeDto) {
        if (changeDto == null) {
            return Collections.emptyList();
        }
        return changeDto.stream()
                .map(this::convertChat)
                .collect(Collectors.toList());
    }

    private Chat convertChat(ChatStatusChangeDto chatStatusChangeDto) {
        return Chat.builder()
                .id(chatStatusChangeDto.getChatId())
                .status(convertStatus(chatStatusChangeDto.getNewStatus()))
                .isAnonymous(chatStatusChangeDto.getNewIsAnonymous())
                .doctor(convertUserById(chatStatusChangeDto.getUserId()))
                .build();
    }

    private ChatStatus convertStatus(String status) {
        if (status == null) {
            return null;
        }
        return ChatStatus.valueOf(status);
    }

    private User convertUser(UserDto userDto) {
        if (userDto == null) {
            return null;
        }
        return User.builder()
                .id(userDto.getId())
                .build();
    }

    private User convertUserById(Integer userId) {
        if (userId == null) {
            return null;
        }
        return User.builder()
                .id(userId)
                .build();
    }

}
