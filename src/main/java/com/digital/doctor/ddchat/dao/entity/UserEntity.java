package com.digital.doctor.ddchat.dao.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "user_info")
@Getter
@Setter
@NoArgsConstructor
public class UserEntity extends BaseEntity {

    @Column(name = "firstname", nullable = false)
    private String firstName;

    @Column(name = "middle_name")
    private String middleName;

    @Column(name = "lastname", nullable = false)
    private String lastName;

    /**
     * Роль пользователя в системе.
     */
    @Column(name = "role", nullable = false)
    private String role;

    @Builder
    public UserEntity(
            Integer id,
            String firstName,
            String middleName,
            String lastName,
            String role
    ) {
        super(id);
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        this.role = role;
    }

}
