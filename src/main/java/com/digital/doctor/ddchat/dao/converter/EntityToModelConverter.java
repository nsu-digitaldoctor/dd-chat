package com.digital.doctor.ddchat.dao.converter;

/**
 * Конвертер сущностей слоя данных в модели.
 *
 * @param <T>
 *     Тип сущности.
 * @param <R>
 *     Тип модели.
 */
public interface EntityToModelConverter<T, R> {

    T toEntity(R model);

    R toModel(T entity);

}
