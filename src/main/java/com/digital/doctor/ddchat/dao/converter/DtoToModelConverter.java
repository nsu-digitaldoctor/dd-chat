package com.digital.doctor.ddchat.dao.converter;

/**
 * Конвертер DTO в модели.
 *
 * @param <T>
 *     Тип DTO.
 * @param <R>
 *     Тип модели.
 */
public interface DtoToModelConverter<T, R> {

    T toDto(R model);

    R toModel(T dto);

}
