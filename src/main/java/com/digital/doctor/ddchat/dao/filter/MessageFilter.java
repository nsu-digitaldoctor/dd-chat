package com.digital.doctor.ddchat.dao.filter;

import com.digital.doctor.ddchat.dao.entity.MessageEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.time.OffsetDateTime;

@Getter
@Builder
@AllArgsConstructor
public class MessageFilter {

    private final Integer messageId;

    /**
     * Идентификатор обсуждения.
     */
    private final Integer chatId;

    /**
     * Идентификатор пользователя.
     */
    private final Integer userId;

    private final OffsetDateTime startTime;
    private final OffsetDateTime endTime;

    public boolean isCorrect(MessageEntity entity) {
        if (messageId != null && !messageId.equals(entity.getId())) {
            return false;
        }

        if (chatId != null && !chatId.equals(entity.getId())) {
            return false;
        }

        if (userId != null && !userId.equals(entity.getUserId())) {
            return false;
        }

        if (startTime != null && !startTime.isBefore(entity.getSentDate())) {
            return false;
        }

        return endTime == null || endTime.isAfter(entity.getSentDate());
    }

}
