package com.digital.doctor.ddchat.dao.converter;

import com.digital.doctor.ddchat.dao.UserDao;
import com.digital.doctor.ddchat.dao.entity.ChatEntity;
import com.digital.doctor.ddchat.model.Chat;
import com.digital.doctor.ddchat.model.User;
import com.digital.doctor.ddchat.model.enums.ChatStatus;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class ChatConverter implements EntityToModelConverter<ChatEntity, Chat> {

    private final UserDao userDao;

    @Override
    public ChatEntity toEntity(Chat model) {
        if (model == null) {
            return null;
        }
        return ChatEntity.builder()
                .id(model.getId())
                .userId(convertUserId(model.getUser()))
                .title(model.getTitle())
                .isAnonymous(model.getIsAnonymous())
                .status(convertStatus(model.getStatus()))
                .doctorId(convertUserId(model.getDoctor()))
                .createDate(model.getCreateDate())
                .build();
    }

    @Override
    public Chat toModel(ChatEntity entity) {
        if (entity == null) {
            return null;
        }
        return Chat.builder()
                .id(entity.getId())
                .user(userDao.getUserById(entity.getUserId()))
                .title(entity.getTitle())
                .isAnonymous(entity.getIsAnonymous())
                .status(convertStatus(entity.getStatus()))
                .doctor(userDao.getUserById(entity.getDoctorId()))
                .createDate(entity.getCreateDate())
                .build();
    }

    private Integer convertUserId(User user) {
        if (user == null) {
            return null;
        }
        return user.getId();
    }

    private String convertStatus(ChatStatus status) {
        if (status == null) {
            return null;
        }
        return status.name();
    }

    private ChatStatus convertStatus(String status) {
        if (status == null) {
            return null;
        }
        return ChatStatus.valueOf(status);
    }

}
