package com.digital.doctor.ddchat.dao.jpa.chat;

import com.digital.doctor.ddchat.dao.entity.ChatEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ChatJpaRepository extends JpaRepository<ChatEntity, Integer> {

    List<ChatEntity> findByUserId(Integer userId);

    List<ChatEntity> findByStatus(String status);

    List<ChatEntity> findByIsAnonymous(Boolean isAnonymous);

}
