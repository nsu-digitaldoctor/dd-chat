package com.digital.doctor.ddchat.configuration;

import com.digital.doctor.ddchat.dto.CreateChatDto;
import com.digital.doctor.ddchat.dto.CreateMessageDto;
import com.digital.doctor.ddchat.dto.DeleteMessageDto;
import com.digital.doctor.ddchat.dto.GetChatListDto;
import com.digital.doctor.ddchat.dto.GetMessageListDto;
import com.digital.doctor.ddchat.dto.UpdateChatDto;
import com.digital.doctor.ddchat.validator.JsonSchemaValidator;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Map;

@Configuration
public class RootConfiguration {

    @Bean
    @Qualifier("chatValidator")
    public JsonSchemaValidator chatValidator(
            ObjectMapper objectMapper) {
        return new JsonSchemaValidator(
                Map.of(
                        CreateChatDto.class,
                        "schemas/create-chat-schema.json",
                        GetChatListDto.class,
                        "schemas/get-chat-list-schema.json",
                        UpdateChatDto.class,
                        "schemas/update-chat-status-schema.json"
                ),
                objectMapper
        );
    }

    @Bean
    @Qualifier("messageValidator")
    public JsonSchemaValidator messageValidator(
            ObjectMapper objectMapper) {
        return new JsonSchemaValidator(
                Map.of(
                        CreateMessageDto.class, "schemas/create-message-schema.json",
                        DeleteMessageDto.class, "schemas/delete-message-schema.json",
                        GetMessageListDto.class, "schemas/get-message-list-schema.json"
                ),
                objectMapper
        );
    }

}
